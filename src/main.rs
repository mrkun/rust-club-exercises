use core::ops::Mul;
use core::ops::MulAssign;

fn main() {
    
    let canonical = [1,1,2,3,5,8,13,21,34,55,89];
    
    let tested_funs 
        // wtf?
        // : [(&fn(usize) -> usize, &str)]
        = 
        [ (fib_recur as fn(usize) -> usize, "direct")
        , (fib_loop  as fn(usize) -> usize, "loop")
        , (fib_monoid as fn(usize) -> usize, "monoidal")
        ];

    for (fun, name) in tested_funs.iter() {
        let pass = (0..10_usize).into_iter().fold(
            true,
            |acc, i| acc && fun(i) == canonical[i]
        );
        if pass {
            println!("\"{}\" passed!", name);
        }
        else {
            println!("\"{}\" failed!", name);
            for i in 0..10 {
                println!(
                    "{} {} {} {}",
                    i, 
                    fun(i), 
                    canonical[i], 
                    fun(i) == canonical[i]
                );
            }
        }
    }
}

fn fib_recur(n: usize) -> usize {
    if n == 0 || n == 1 {
        return 1
    } else {
        return fib_recur(n - 1) + fib_recur(n - 2)
    }
}


fn fib_loop(n: usize) -> usize {
    let (mut a, mut b) = (0, 1);
    for _ in 0..n {
        // Requires nightly rust for the unstable destructuring assignment
        // (a, b) = (b, a+b);
        let b_ = a + b;
        a = b;
        b = b_;
    }
    return b;
}

#[allow(non_camel_case_types)]
#[derive(Copy, Clone)]
struct Mat_2_2 {
    x_0_y_0: usize,
    x_0_y_1: usize,
    x_1_y_0: usize,
    x_1_y_1: usize,
}

impl Mul for Mat_2_2 {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        return Mat_2_2 {
            x_0_y_0: other.x_0_y_0 * self.x_0_y_0 + other.x_1_y_0 * self.x_0_y_1,
            x_0_y_1: other.x_0_y_1 * self.x_0_y_0 + other.x_1_y_1 * self.x_0_y_1,
            x_1_y_0: other.x_0_y_0 * self.x_1_y_0 + other.x_1_y_0 * self.x_1_y_1,
            x_1_y_1: other.x_0_y_1 * self.x_1_y_0 + other.x_1_y_1 * self.x_1_y_1,
        };
    }
}

impl MulAssign for Mat_2_2 {
    fn mul_assign(&mut self, other: Self) {
        *self = *self * other;
    }
}

const UNIT : Mat_2_2 = Mat_2_2 {
    x_0_y_0: 1,
    x_0_y_1: 0,
    x_1_y_0: 0,
    x_1_y_1: 1,
};

fn mat_pow(a: Mat_2_2, n: usize) -> Mat_2_2 {
    if n == 0 {
        return UNIT;
    }
    else if n == 1 {
        return a;
    }
    else {
        let mut b = a;
        let mut rest = n;
        let mut res = UNIT;
        while rest > 0 {
            if rest % 2 != 0 {
                res *= b
            }
            rest >>= 1;
            b = b * b;
        }
        return res;
    }
}

fn fib_monoid(n: usize) -> usize {
    let base = Mat_2_2 { 
        x_0_y_0: 0,
        x_0_y_1: 1,
        x_1_y_0: 1,
        x_1_y_1: 1,
    };

    let res = mat_pow(base, n);

    return res.x_1_y_1;
}
